from sqlmodel import Session, select
from models.wallet_model import Wallets, CreateWallet
from data.database import ENGINE
from fastapi.responses import JSONResponse
from utils.uuid_generator import generate_uuid
from models.user_model import UsersDB
from models.wallets_has_users_model import WalletsHasUsers
from models.bank_card_model import BankCardsDB
from models.transfer_model import Transfer, TransferMoney, Withdraw
from datetime import datetime

def get_all_wallets():
    """Return all wallets"""
    with Session(ENGINE) as session:
        statement = select(Wallets)
        wallets = session.exec(statement).all()
        return wallets


def get_by_id(id: str):
    """Return Wallet using Wallets.id
    Args:
        id (str): UUID
    Returns:
        Wallets model
    """
    with Session(ENGINE) as session:
        statement = select(Wallets).where(Wallets.id == id)
        wallet = session.exec(statement)
        return wallet.first()



def create_wallet(new_wallet: CreateWallet, admin_id: str):
    """User add wallet name and currency
    Args:
        new_wallet (CreateWallet): 
        admin_id (str):UUID authenticated user
    Returns:
        Wallets model
    """
    wallet = Wallets(id = str(generate_uuid()),
                     wallet_name=new_wallet.wallet_name,
                     amount=0,
                     currency=new_wallet.currency,
                     admin_id=admin_id)
    user = UsersDB(id=admin_id)
    relation = WalletsHasUsers(wallet_id=wallet.id, user_id=user.id)
    with Session(ENGINE) as session:
        session.add(wallet)
        session.add(relation)
        session.commit()
        session.refresh(wallet)
        return wallet


def add_amount_in_wallet(user_id: str, wallet_id: str, changed_wallet: TransferMoney):
    """The function adding amount in current wallet from bank card
    Args:
        user_id (str): UUID Authenticated user
        wallet_id (str): 
        changed_wallet (TransferMoney): 
    Returns:
        Transfer model
    """
    with Session(ENGINE) as session:         
        statement = select(Wallets).where(Wallets.id == wallet_id)
        results = session.exec(statement)
        wallet = results.one()
        wallet.amount += changed_wallet.amount
        transfer = Transfer(user_id=user_id,
                            wallets_id=wallet.id,
                            bank_cards_id=changed_wallet.bank_card,
                            amount=changed_wallet.amount,
                            time=datetime.now())
        session.add(wallet)
        session.add(transfer)
        session.commit()
        session.refresh(wallet)
        session.refresh(transfer)
        return transfer

# def add_amount_in_wallet(wallet_id: str, changed_wallet: TransferMoney):
#     with Session(ENGINE) as session:         
#         statement = select(Wallets).where(Wallets.id == wallet_id)
#         results = session.exec(statement)
#         wallet = results.one()
#         wallet.amount += changed_wallet.amount
#         session.add(wallet)
#         session.commit()
#         session.refresh(wallet)


def delete_wallet(id: str):
    """Delete wallet useng wallet id
    Args:
        id (str): UUID
    """
    with Session(ENGINE) as session:
        delete_wallets_location_statement = session.query(WalletsHasUsers).filter_by(wallet_id=id).delete()
        delete_statement = session.query(Wallets).filter_by(id=id).delete()
        if delete_statement and delete_wallets_location_statement:
            session.commit()
            return JSONResponse(status_code=200, content='Wallet deleted!')


def get_all_wallets_for_auth_user(user_id: str):
    """The function return all wallets for authenticated user
    Args:
        user_id (str):UUID Authenticated user id
    Returns:
        Wallets model
    """
    with Session(ENGINE) as session:
        statement = select(Wallets).where(Wallets.admin_id == user_id)
        wallets = session.exec(statement)
        return wallets.all()


def get_all_wallet_ids_for_auth_user(user_id: str) -> list:
    """Return list of all wallet ids for authenticated user
    Args:
        user_id (str):UUID Authenticated user
    Returns:
        list: Wallets.id
    """
    with Session(ENGINE) as session:
        statement = select(Wallets.id).where(Wallets.admin_id == user_id)
        wallets = session.exec(statement)
        return wallets.all()


def get_all_bank_card_ids_for_auth_user(user_id: str) -> list:
    """Return list of all bank card ids for authenticated user
    Args:
        user_id (str):UUID Authenticated user
    Returns:
        list: BankCardsDB.id
    """
    with Session(ENGINE) as session:
        statement = select(BankCardsDB.id).where(BankCardsDB.user_id == user_id)
        wallets = session.exec(statement)
        return wallets.all()


def wallet_withdraw(user_id: str, wallet_id: str, changed_wallet: TransferMoney):
    """The function withdraw amount from current wallet
    Args:
        user_id (str): Authenticated user
        wallet_id (str): UUID
        changed_wallet (TransferMoney):
    Returns:
        Transfer model
    """
    with Session(ENGINE) as session:
        statement = select(Wallets).where(Wallets.id == wallet_id)
        results = session.exec(statement)
        wallet = results.one()
        wallet.amount -= changed_wallet.amount
        transfer = Transfer(user_id=user_id,
                            wallets_id=wallet.id,
                            bank_cards_id=changed_wallet.bank_card,
                            amount=-changed_wallet.amount,
                            time=datetime.now())
        session.add(wallet)
        session.add(transfer)
        session.commit()
        session.refresh(wallet)
        session.refresh(transfer)
        return transfer


def get_all_transfers_for_auth_user(user_id: str):
    """Return all transfers for authenticated user
    Args:
        user_id (str): UUID Authenticated user
    Returns:
        Transfer model
    """
    with Session(ENGINE) as session:
        statement = select(Transfer).where(Transfer.user_id == user_id)
        transfer = session.exec(statement).all()
        return transfer


def delete_all_transfers_for_auth_user(user_id: str):
    """Delete all transfers from database using authenticated user id
    Args:
        user_id (str): UUID Authenticated user
    """
    with Session(ENGINE) as session:
        session.query(Transfer).where(Transfer.user_id == user_id).delete()
        session.commit()


def get_wallet_amount_for_auth_user(user_id: str, wallet_id: str) -> float:
    """Return amount for current wallet using wallet id and authenticated user id
    Args:
        user_id (str):UUID Authenticated user
        wallet_id (str): UUID
    Returns:
        float: Wallets.amount
    """
    with Session(ENGINE) as session:
        statement = select(Wallets.amount).where(Wallets.id == wallet_id).where(Wallets.admin_id==user_id)
        wallet = session.exec(statement).first()
        return wallet
    

def check_if_user_owns_wallet(wallet_id, user_id) -> bool | Wallets:
    wallet = get_by_id(wallet_id)

    if not wallet:
        return False

    user_wallets_ids = get_all_wallet_ids_for_auth_user(user_id)

    if wallet.id not in user_wallets_ids:
        return False

    return wallet
