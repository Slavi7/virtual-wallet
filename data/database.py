from sqlmodel import create_engine
import mysql.connector
import os

# Get your environment Mailjet keys
HOST = os.environ['lin_host']
USER = os.environ['lin_user']
PASSWORD = os.environ['lin_password']
DATABASE = os.environ['lin_database']

connection = mysql.connector.connect(
    host=HOST,
    user=USER,
    password=PASSWORD,
    database=DATABASE
)


ENGINE = create_engine("mysql+mysqlconnector://", creator=lambda: connection)
