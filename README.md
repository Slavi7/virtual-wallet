# Backend API Project - Virtual Wallet

### Developed by: Mihail Gabrovski, Svetoslav Dimitrov, Tsvyatko Petrov

## Description:

RESTful API which can be consumed by different clients.
Basic description:
 - User can create new registration;
 - User can add bank cards and create multiple virtual wallets (also in different currencies);
 - User can fund his own virtual wallets via bank card/s;
 - User can transfer money from his wallet to another.
 - Users can view their own transaction and transfers.
 - Administrators can manage all users, block user, view transactions and transfers.

The project is fully implemented with FastApi framework and for Database is used MySQL(we used Akamai's Linode cloud service for the database). Key libraries, used in the project:
- fastapi==0.95.2
- pydantic==1.10.7
- uvicorn==0.22.0
- sqlmodel==0.0.8
- bcrypt==4.0.1
- PyJWT==2.7.0
- mailjet-rest==1.3.4
- mysql-connector-python==8.0.33
- mysqlclient==2.1.1
- uvicorn==0.22.0

## Endpoints descriptions:

### Public part

#### POST /users/:
    - Description:
        - Can register new user.
    - request body(required): 
        - username (str, min_len = 2, max_len = 20),
        - password (str, min_len = 8, max_len = 20, must have upper case, lower case, number, special symbol),
        - cofirm_password (str, min_len = 8, max_len = 20, must have upper case, lower case, number, special symbol),
        - phone_number (str, min_len 10, max_len 10)
        - email (valid email)
    - restrictions:
        - username must be unique;
        - email must be unique;
        - phone number must be unique;
        - password and cofirm_password must be equal;
    - Possible outcomes:
        - on success: status code = 201 with additional message;
        - validation error: status code = (422,409,403), additional information;

#### GET /users/token:
    - Description:
        - Retrive validation token.
    - request body (required): 
        - **username**,
        - **password**,
    - restrictions:
        - username must be already registered user;
    - Possible outcomes:
        - on success: status code = 200, returns validation token as a __str__;
        - validation error: status code = (422,400), additional information;

#### PATCH /users/{username}/confirm:
    - Description:
        - User can confirm his registration email.
    - Possible outcomes:
        - on success: status code = 200;
        - validation error: status code = (422), additional information;

#### GET /users/{username}/confirm:
    - Description:
        - User can confirm his registration email (via browser). This endpoints mimics the function in PATCH /users/{username}/confirm, so when the user receive email with validation link to be able to validate it by clicking on the link.
    - Possible outcomes:
        - on success: status code = 200;
        - validation error: status code = (422), additional information;

### Private Part - User 

#### GET /users/me
    - Description:
        - Return JSON response with logged user information. 
    - Possible outcomes:
        - on success: status code = 200;
        - validation error: status code = (422), additional information;

#### PATCH /users/me
    - Description:
        - User can update his phone number, email or password. 
    - Possible outcomes:
        - on success: status code = 200;
        - validation error: status code = (422), additional information;

#### PATCH /users/me/deactivate
    - Description:
        - User can deactivate his profile in the virtual wallet app. 
    - Possible outcomes:
        - on success: status code = 200;

#### GET /users/me/bank_cards
    - Description:
        - User can view list of all added bank cards with additional information for every bank card. 
    - Possible outcomes:
        - on success: status code = 200;

#### POST /users/me/bank_cards
    - Description:
        - User can add bank card in his profile.
    - Possible outcomes:
        - on success: status code = 200;
        - validation error: status code = (422), additional information;

#### DELETE /users/me/bank_cards
    - Description:
        - User can delete bank card from his profile.
    - Possible outcomes:
        - on success: status code = 200;
        - validation error: status code = (422), additional information;

#### GET /wallets/{id}
    - Description:
        - User can view all information about his wallet with this ID.
    - Possible outcomes:
        - on success: status code = 200;
        - validation error: status code = 400 (when Id is not 36 characters), status code = 404 (when ID not in current user wallet ids)


#### DELETE /wallets/{id}
    - Description:
        - User delete wallet with this ID
    - Possible outcomes:
        - on success: status code = 200;
        - validation error: status code = 400 (when Id is not 36 characters), status code = 404 (when ID not in current user wallet ids)

        
#### GET /wallets/
    - Description:
        - User can view all his wallets and info about them
    - Possible outcomes:
        - on success: status code = 200;
        - validation error:

#### POST /wallets/
    - Description:
        - User can create new wallet using wallet_name and currency:
        wallet_name: str = Field(min_length=3, max_length=45) - must be between 3 and 45 characters
        currency: Literal[lst] = 'BGN'  - user can choose from list with currencies. 'BGN' is default currency
        id - auto increment
        amount - default value is 0
        admin_id - Authenticated user ID
    - Possible outcomes:
        - on success: status code = 201
        - validation error:
#### GET /categories/
    - Description:
        - User can view all categories
    - Possible outcomes:
        - on success: status code = 200;
        - validation error:
#### POST /categories/
    - Description:
        - User can create a new category using category:
        category: str = Field(min_length=3, max_length=45)
        id - auto increment
    - Possible outcomes:
        - on success: status code = 201 with message "Category created";
        - validation error:
#### PUT /categories/{id}
    - Description:
        - User can change category name
        category: str = Field(min_length=3, max_length=45)
    - Possible outcomes:
        - on success: status code = 200 with message Category updated;
        - validation error:
#### DELETE /categories/{id}
    - Description:
        - User can delete category using category ID
    - Possible outcomes:
        - on success: status code = 200 with message "Category deleted";
        - validation error: status code = 404 if ID not exist
#### GET /categories/{category_id}/total_sum
    - Description:
        - User can view total sum from all transactions in this category using category ID
    - Possible outcomes:
        - on success: status code = 200 with message "Total sum in this category is: 500 (for example)";
        - validation error: status code 404 if category ID not exist
#### POST /transactions/
    - Description:
        - User can create transaction using amount: float = Field(gt=0), category: str, sender_wallet_id: str,
        receiver_wallet_id: str. By using the query parameters the receiver will be found.
    - Possible outcomes:
        - on success: status code = 201;
        - validation error: status code = 404 if wallet not found, status code = 400 if other validations fail

#### PUT /transactions/{txn_id}/confirm
    - Description:
        - User can confirm the transaction using the transacton ID. The balance of the  wallet will be reduced by the 
        corresponding amount and the status of the transaction will be set to 'pending'.
    - Possible outcomes:
        - on success: status code = 200, with message 'Transaction confirmed';
        - validation error: status code = 404 if transaction not found

#### PUT /transactions/{txn_id}/edit
    - Description:
        - User can choose different wallet, can change the receiver, the amount and the category.
    - Possible outcomes:
        - on success: status code = 200 and returns the transaction;
        - validation error: status code = 404 if wallet not found; status code = 400 if other validations fail

#### GET /transactions/pending
    - Description:
        - User can see theirs pendng transactions
    - Possible outcomes:
        - on success: status code = 200;
        - validation error:

#### PATCH /transactions/pending/{txn_id}/accept
    - Description:
        - User can accept pending transaction using transaction ID. The balance of their wallet will be increased with the corresponding 
        amount and the status of the transaction will be set to 'accepted'.
    - Possible outcomes:
        - on success: status code = 202 with message 'Transaction accepted'; status code = 404 if transaction not found,
        status code = 400 if transaction already processed.
        - validation error:

#### PATCH /transactions/pending/{txn_id}/decline
    - Description:
        - User can decline the pending transaction using transaction ID. The money will be returned to the balance ot the senders wallet
        and the status of the transaction will be set to 'declined'.
    - Possible outcomes:
        - on success: status code = 200 with message 'Transaction declined';
        - validation error: status code = 404, with message 'Transaction not found'.

#### GET /transactions/all
    - Description:
        - User can view all of their transactions. Can be filtered by: "period", "recipient_id", "direction" and can be 
        sorted by "amount" or "time" ascending or descending. 
    - Possible outcomes:
        - on success: status code = 200;
        - validation error: status code = 422 if searched direction is wrong.

#### PUT /transfers/{wallet_id}/amount
    - Description:
        - User can transfer amout from some of his bank cards to wallet using:
        wallet id
        amount
        bank_card id
    - Possible outcomes:
        - on success: status code = 200 with message "The sum: 100.0 succesfully added in your wallet";
        - validation error:status code 404 if wallet id not exist, 404 if bank_card id not exist

#### PUT /transfers/{wallet_id}/withdraw
    - Description:
        - User can withdraw amount from some of his wallets to some of his bank cards using:
        wallet id
        amount
        bank_card id
    - Possible outcomes:
        - on success: status code = 200 with message "The sum: 100.0 successfully withdrawn from your wallet";
        - validation error: status code 404 if wallet id not exist, 404 if bank card id not exist

#### GET /transfers/
    - Description:
        - User can view all his transfers from bank cards to wallets and from wallets to bank cards. 
        The info contain information about:
        transfer_id
        user_id
        wallet_id
        bank_card_id
        amount (possitive int if operation is add amount to wallet or negative int if operation is withdraw)
        time (exact date and time the operation was performed)
    - Possible outcomes:
        - on success: status code = 200;
        - validation error:

#### DELETE /transfers/
    - Description:
        - User can delete all transfer info
    - Possible outcomes:
        - on success: status code = 200;
        - validation error:

#### POST /contacts/from_all_users
    - Description:
        - User can add other user to a contact list using username. The user can choose from all users who has registered in Virtual Wallet App
    - Possible outcomes:
        - on success: status code = 200 with message: User with username {friend_name} added in to your contact list!'
        - validation error: status code 404 if username not exist with additional message, 
        status code 400 if username is already in contact list with additional message

#### POST /contacts/from_transactions
    - Description:
        - User can add other user to a contact list using username. Тhe user can choose from all the users with whom he has had transactions.
    - Possible outcomes:
        - on success: status code = 200 User with username {friend_name} added in to your contact list!'
        - validation error: status code 404 if username not in transactions user list and additional message,
        status code 400 if username is already in contact list with additional message

#### DELETE /contacts/{username}
    - Description:
        - User can delete other user from his contact list using username
    - Possible outcomes:
        - on success: status code = 200 and message: 'User {username} succesfully deleted from your contact list!'
        - validation error: status code 404 if user name not in contact list and message: 'User with username {username} not in your contact list!'

#### GET /contacts/all_friends
    - Description:
        - User can view a list of all friend usernames
    - Possible outcomes:
        - on success: status code = 200;
        - validation error:
