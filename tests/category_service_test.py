import unittest
from unittest.mock import Mock, patch
from fastapi.responses import JSONResponse
from services import category_service
from models.category_model import Categories, CreateCategory, UpdateCategory
from sqlmodel import create_engine, Session
from models.transaction_model import Transactions



fake_engine = create_engine(f"sqlite:///fake_file_name", echo=True)

FAKE_CATEGORY = Categories(id=1, category='FAKE1')
FAKE_CATEGORY_NAMES = [Categories(category='FAKE1'), Categories(category='FAKE3')]
FAKE_CREATE_CATEGORY = CreateCategory(category='FAKE category')
FAKE_UPDATE_CATEGORY = UpdateCategory(category='fake update category')
FAKE_CATEGORIES = [Categories(id=1, category='FAKE1'), Categories(id=3, category='FAKE3')]
FAKE_TRANSACTIONS_AMOUNT = [500.00, 200.00]
FAKE_USER_ID = '20bc6d1e-9cc2-4578-baea-52db642d7d5d'
FAKE_CATEGORY_ID = 14



class FakeResult:
    val = []

    @classmethod
    def all(cls):
        return cls.val

    @classmethod
    def first(cls):
        return cls.val


class FakeQuery():
    
    def query():
        pass
    
    def filter_by():
        pass
    
    def delete():
        pass

    def filter():
        pass

    def one():
        pass



class CategoryService_Should(unittest.TestCase):


    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=[]))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_category_names_if_no_categories(self):
        result: list[Categories.category] = category_service.get_all_category_names()
        self.assertEqual(result, [])



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=['str1', 'str2']))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_category_names_if_exist(self):
        result: list[Categories.category] = category_service.get_all_category_names()
        self.assertEqual(len(result), 2)



    @patch.object(Session, "refresh", Mock(return_value=FAKE_CATEGORY))
    @patch.object(Session, "add", Mock(return_value="ok"))
    @patch.object(Session, "commit", Mock(return_value="ok"))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_create_category(self):
        result: Categories = category_service.create_category(FAKE_CREATE_CATEGORY)
        self.assertIsInstance(result, Categories)



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value=(FAKE_CATEGORIES[0])))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_category_by_id_return_category_if_id_exist(self):
        result = category_service.get_category_by_id(1) 
        self.assertEqual(result, FAKE_CATEGORY)



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value=(FAKE_CATEGORIES[0])))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_category_by_id_return_category_if_isistance(self):
        result = category_service.get_category_by_id(1) 
        self.assertIsInstance(result, Categories)



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value= None))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_category_by_id_return_None_if_category_not_exist(self):
        result: Categories = category_service.get_category_by_id(3)
        self.assertEqual(result, None)



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=[]))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_categories_if_no_categories(self):
        result: list[Categories] = category_service.get_all_categories()
        self.assertEqual(result, [])



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=FAKE_CATEGORIES))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_categories_if_exist(self):
        result: list[Categories] = category_service.get_all_categories()
        self.assertEqual(len(result), 2)



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=FAKE_CATEGORIES))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_all_categories_if_isIstance(self):
        result: Categories = category_service.get_all_categories()
        self.assertIsInstance(result, list)



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value= None))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_category_by_name_return_None_if_category_not_exist(self):
        result: Categories.category = category_service.get_category_by_name('fake')
        self.assertEqual(result, None)


    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "first", Mock(return_value= FAKE_CATEGORIES[0]))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_category_by_name_if_category_exist(self):
        result: Categories.category = category_service.get_category_by_name('FAKE1')
        self.assertEqual(result, FAKE_CATEGORY)



    @patch.object(Session,"query", Mock(return_value=FakeQuery()))
    @patch.object(FakeQuery,"filter_by", Mock(return_value=FakeQuery()))
    @patch.object(FakeQuery,"delete", Mock(return_value=1))
    @patch.object(Session,"commit", Mock(return_value="ok"))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_delete_category_by_id(self):
        result: Categories = category_service.delete_category_by_id(id=1)
        self.assertIsInstance(result, JSONResponse)
        self.assertEqual(result.status_code, 200)




    @patch.object(Session, "query", Mock(return_value=FakeQuery()))
    @patch.object(FakeQuery, "filter", Mock(return_value=FakeQuery()))
    @patch.object(FakeQuery, "one", Mock(return_value=FAKE_CATEGORY))
    @patch.object(Session, "refresh", Mock(return_value=FAKE_CATEGORY))
    @patch.object(Session, "commit", Mock(return_value="ok"))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_update_category(self):
        result: Categories = category_service.update_category(1, FAKE_UPDATE_CATEGORY)
        self.assertIsInstance(result, Categories)



    @patch.object(Session, "exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult, "all", Mock(return_value=FAKE_TRANSACTIONS_AMOUNT))
    @patch("services.category_service.ENGINE", Mock(return_value=fake_engine))
    def test_get_transactions_sum_for_category_return_list_of_floats(self):     
        result: list[Transactions.amount] = category_service.get_transactions_sum_for_category(FAKE_USER_ID, FAKE_USER_ID)
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)





