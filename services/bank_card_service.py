from sqlalchemy import exc
from sqlmodel import Session, select
from models.bank_card_model import BankCardAdd, BankCardsDB
from data.database import ENGINE
from fastapi.responses import JSONResponse
from models.user_model import UserCredentials


def create(card: BankCardAdd, user: UserCredentials):
    db_bank_card = BankCardsDB(card_number=card.card_number, card_holder=card.card_holder,
                               ccv=card.ccv, expiration=card.expiration,
                               user_id=user.id, currency=card.currency)
    try:
        with Session(ENGINE) as session:
            session.add(db_bank_card)
            session.commit()
            session.refresh(db_bank_card)
    except exc.IntegrityError as e:
        return JSONResponse(status_code=400, content={"msg": e.args[0]})

    return db_bank_card


def get_card_by_id(id):
    with Session(ENGINE) as session:
        statement = select(BankCardsDB).where(BankCardsDB.id == id)
        card = session.exec(statement).first()

        return card


def get_cards_by_user(user: UserCredentials):
    with Session(ENGINE) as session:
        statement = select(BankCardsDB).where(BankCardsDB.user_id == user.id)
        cards = session.exec(statement).all()

        return cards


def exists(id):
    with Session(ENGINE) as session:
        statement = select(BankCardsDB).where(BankCardsDB.id == id)
        card = session.exec(statement).first()

    return card


def delete(id, user: UserCredentials):
    user_cards = get_cards_by_user(user)
    card_to_be_deleted = exists(id)

    if card_to_be_deleted not in user_cards:
        return JSONResponse(status_code=403, content='User is not the owner of the card')

    with Session(ENGINE) as session:
        statement = select(BankCardsDB).where(BankCardsDB.id == id)
        card = session.exec(statement).one()
        session.delete(card)
        session.commit()

    return JSONResponse(status_code=200, content=f'Bank card {id} deleted')
