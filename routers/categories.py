from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from data.database import ENGINE
from models.category_model import CreateCategory, UpdateCategory
from services import category_service
from services import user_service
from models.user_model import UserCredentials



categories_router = APIRouter(prefix='/categories')


@categories_router.post('/', tags=["Private part"])
def create_category(new_category: CreateCategory,
                    current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    else:
        if len(new_category.category) < 3 or len(new_category.category) > 45:
            return JSONResponse(status_code=400,
                                content='Category name must be between 3 and 45 characters!')
        all_categories = category_service.get_all_category_names()
        if new_category.category in all_categories:
            return JSONResponse(status_code=400, content='Category with this name already exist!')
        category_service.create_category(new_category)
        return JSONResponse(status_code=200, content=f'Category with name {new_category.category} created!')



@categories_router.delete('/{id}', tags=["Private part"])
def delete_category(id: int,
                    current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    else:
        existing_category = category_service.get_category_by_id(id)
        if existing_category is None:
            return JSONResponse(status_code=404, content='Category with this id not exist!')
        return category_service.delete_category_by_id(id)
    


@categories_router.put('/{id}', tags={"Private part"})
def update_category(id: int, new_category: UpdateCategory,
                    current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    else:
        existing_category = category_service.get_category_by_id(id)
        if existing_category is None:
            return JSONResponse(status_code=404, content='Category with this id not exist!')
        all_categories = category_service.get_all_category_names()
        if new_category.category in all_categories:
            return JSONResponse(status_code=400, content='Category with this name already exist!')
        category_service.update_category(id, new_category)
        return JSONResponse(status_code=200, content='Category updated!')



@categories_router.get('/', tags=["Private part"])
def get_all(current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    return category_service.get_all_categories()
    


@categories_router.get('/{category_id}/total_sum', tags=["Private part"])
def get_total_sum_in_category(category_id: int,
                              current_user: UserCredentials = Depends(user_service.get_current_user)):
    if current_user is None:
        return JSONResponse(status_code=404, content='User not found!')
    user_id = current_user.id
    existing_category = category_service.get_category_by_id(category_id)
    if existing_category is None:
        return JSONResponse(status_code=404, content='Category with this ID not found')
    else:
        payments = category_service.get_transactions_sum_for_category(category_id, user_id)
        total_sum = sum(payments)
        return JSONResponse(status_code=200, content=f'Total sum in this category is: {total_sum}')
