from mailjet_rest import Client
import os

# Get your environment Mailjet keys
API_KEY = os.environ['MJ_APIKEY_PUBLIC']
API_SECRET = os.environ['MJ_APIKEY_PRIVATE']


mailjet = Client(auth=(API_KEY, API_SECRET), version="v3.1")


def email_confirmation(template_id: int, subject: str, receiver_email: str, reveicer_name: str, variables: dict = None):
    data = _email_template(template_id, subject, receiver_email, reveicer_name, variables)
    print(data)
    result = mailjet.send.create(data=data)

    return result.status_code


def _email_template(template_id: int, subject: str, receiver_email: str, receiver_name: str, variables: dict = None) -> dict:
    """Return email template with pre-defined sender. Can be used only with existing Mailjet templete.
      
    Args:
        template_id (int): Mailjet template ID. Must be created in the admin profile.
        subject (str): Email subject.
        receiver_email (str): Email of the reveiver.
        receiver_name (str): Receiver's name or username.
        variables (dict, optional): Used if pre-defined Mailjet template needs to receive variables. Defaults to None.

    Returns:
        dict: Dict object with the data needed to sent email successfully.
    """

    data = {
    'Messages': [
        {
        "From": {
            "Email": "virtual.wallet.t.7@gmail.com",
            "Name": "Virtual Wallet"
        },
        "To": [
            {
            "Email": "virtual.wallet.t.7@gmail.com",
            "Name": receiver_name
            }
        ],
        "TemplateID": template_id,
        "TemplateLanguage": True,
        "Subject": subject,
        "Variables": variables
        }
    ]
    }
    
    return data