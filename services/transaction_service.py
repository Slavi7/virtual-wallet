from datetime import datetime
from utils.currencies import get_exc_rate, calculate_conversion_amount
from sqlmodel import Session, select, or_
from models.transaction_model import Transactions, CreateTransaction, UpdateTransaction, ViewTransaction
from models.wallet_model import Wallets
from data.database import ENGINE
from fastapi.responses import JSONResponse
from models.user_model import UserCredentials, UserInfo
from services import wallet_service, user_service, category_service


def prepare_transaction(transaction: CreateTransaction, user: UserCredentials, search_by,
                        val) -> CreateTransaction | JSONResponse:
    wallet = wallet_service.get_by_id(transaction.sender_wallet_id)

    if not wallet:
        return JSONResponse(status_code=404, content='Wallet not found!')

    user_wallets_ids = wallet_service.get_all_wallet_ids_for_auth_user(user.id)

    if wallet.id not in user_wallets_ids:
        return JSONResponse(status_code=404, content='Wallet not found!')

    receiver = user_service.get_user(search_by, val)

    if not receiver:
        return JSONResponse(status_code=400, content='No such receiver')

    if receiver[0].is_blocked or receiver[0].is_inactive:
        return JSONResponse(status_code=400, content=f'{receiver.username} can\'t receive money due to being blocked '
                                                     f'or inactive!')
    transaction.receiver_id = receiver[0].id
    receiver_wallet = wallet_service.check_if_user_owns_wallet(transaction.receiver_wallet_id, transaction.receiver_id)

    if not receiver_wallet:
        return JSONResponse(status_code=404, content='Wallet not found!')

    if wallet.amount < transaction.amount:
        return JSONResponse(status_code=400, content='Amount in the wallet is not sufficient!')

    transaction.exchange_rate = get_exc_rate(wallet.currency, receiver_wallet.currency)

    db_transaction = Transactions(sender_id=user.id, receiver_id=transaction.receiver_id, amount=transaction.amount,
                                  sender_wallet_id=transaction.sender_wallet_id,
                                  receiver_wallets_id=transaction.receiver_wallet_id, time=datetime.now(),
                                  currency=wallet.currency, exchange_rate=transaction.exchange_rate)

    if transaction.category:
        category = category_service.get_category_by_name(transaction.category)

        if not category:
            return JSONResponse(status_code=400, content='No such category!')
        db_transaction.category_id = category.id

    with Session(ENGINE) as session:
        session.add(db_transaction)
        session.commit()
        session.refresh(db_transaction)
        transaction.id = db_transaction.id

    return transaction


def confirm(txn_id: str, user: UserCredentials):
    with Session(ENGINE) as session:
        statement = select(Transactions).where(Transactions.id == txn_id)
        transaction = session.exec(statement).first()

        if transaction is None or transaction.sender_id != user.id or transaction.status == 'pending':
            return JSONResponse(status_code=404, content='Transaction not found')

        transaction.status = 'pending'
        session.add(transaction)
        session.commit()
        session.refresh(transaction)

        statement = select(Wallets).where(Wallets.id == transaction.sender_wallet_id)
        sender_wallet = session.exec(statement).first()
        sender_wallet.amount -= transaction.amount
        session.add(sender_wallet)
        session.commit()
        session.refresh(sender_wallet)

    return JSONResponse(status_code=200, content='Transaction confirmed')


def get_by_id(txn_id: str) -> Transactions:
    with Session(ENGINE) as session:
        statement = select(Transactions).where(Transactions.id == txn_id)
        transaction = session.exec(statement).first()

    return transaction


def update(txn_id: str, update_txn: UpdateTransaction, user: UserCredentials, search_by, val):
    transaction = get_by_id(txn_id)

    if not transaction:
        return JSONResponse(status_code=404, content='Transaction not found!')

    if search_by and val:
        receiver = user_service.get_user(search_by, val)[0]

        if not receiver:
            return JSONResponse(status_code=400, content='No such receiver')

        if receiver.is_blocked or receiver.is_inactive:
            return JSONResponse(status_code=400,
                                content=f'{receiver.username} can\'t receive money due to being blocked '
                                        f'or inactive!')
        transaction.receiver_id = receiver.id

    # if wallet not changed, get the original wallet in order to perform amount validation later
    wallet = wallet_service.get_by_id(transaction.sender_wallet_id)
    # if wallet has been changed perform all the necessary validation for the new wallet
    if update_txn.sender_wallet_id:

        wallet = wallet_service.get_by_id(update_txn.sender_wallet_id)

        if not wallet:
            return JSONResponse(status_code=404, content='Wallet not found!')

        user_wallets = wallet_service.get_all_wallet_ids_for_auth_user(user.id)

        if wallet.id not in user_wallets:
            return JSONResponse(status_code=404, content='Wallet not found!')

        if wallet.amount < transaction.amount:
            return JSONResponse(status_code=400, content='Amount in the wallet is not sufficient!')

        transaction.sender_wallet_id = update_txn.sender_wallet_id
        receiver_wallet = wallet_service.get_by_id(transaction.receiver_wallets_id)
        transaction.exchange_rate = get_exc_rate(wallet.currency, receiver_wallet.currency)

    if update_txn.amount:
        if wallet.amount < update_txn.amount:
            return JSONResponse(status_code=400, content='Amount in the wallet is not sufficient!')

        transaction.amount = update_txn.amount

    if update_txn.category:
        new_category = category_service.get_category_by_name(update_txn.category)

        if not new_category:
            return JSONResponse(status_code=400, content='No such category!')

        transaction.category_id = new_category.id

    with Session(ENGINE) as session:
        session.add(transaction)
        session.commit()
        session.refresh(transaction)

    return transaction


def get_pending_transactions_auth_user(user: UserCredentials):
    with Session(ENGINE) as session:
        statement = select(Transactions).where(Transactions.receiver_id == user.id).where(Transactions.status == 'pending')
        pending_transactions = session.exec(statement).all()

    return pending_transactions


def accept_transaction(txn_id, user: UserCredentials):

    transaction = get_by_id(txn_id)

    if not transaction:
        return JSONResponse(status_code=404, content='Transaction not found!')

    if transaction.status != 'pending':
        return JSONResponse(status_code=400, content='Transaction already processed')

    wallet = wallet_service.get_by_id(transaction.receiver_wallets_id)

    with Session(ENGINE) as session:
        statement = select(Transactions).where(Transactions.id == transaction.id)
        tnx = session.exec(statement).first()
        tnx.status = 'accepted'
        session.add(tnx)
        session.commit()

        statement = select(Wallets).where(Wallets.id == wallet.id)
        upd_wallet = session.exec(statement).first()
        upd_wallet.amount += calculate_conversion_amount(transaction.amount, transaction.exchange_rate)
        session.add(upd_wallet)
        session.commit()

    return JSONResponse(status_code=202, content=f'Transaction accepted.')


def decline_transaction(txn_id, user: UserCredentials):

    with Session(ENGINE) as session:
        statement = select(Transactions).where(Transactions.id == txn_id)
        transaction = session.exec(statement).first()

        if transaction is None or transaction.receiver_id != user.id or transaction.status != 'pending':
            return JSONResponse(status_code=404, content='Transaction not found')

        transaction.status = 'declined'
        session.add(transaction)
        session.commit()
        session.refresh(transaction)

        statement = select(Wallets).where(Wallets.id == transaction.sender_wallet_id)
        sender_wallet = session.exec(statement).first()
        sender_wallet.amount += transaction.amount
        session.add(sender_wallet)
        session.commit()
        session.refresh(sender_wallet)

    return JSONResponse(status_code=200, content='Transaction declined')


def get_auth_user_txns(user: UserCredentials, search_by, val, sort_by, rev) -> list[ViewTransaction]:

    if search_by == 'period':

        if sort_by:
            return sorted(get_user_txns_filtered_by_per(user, val), key=lambda t: t.amount if sort_by == 'amount' else t.time, reverse=rev)

        return get_user_txns_filtered_by_per(user, val)

    if search_by == 'recipient_id':

        if sort_by:
            return sorted(get_user_txns_filtered_by_recipient(user, val), key=lambda t: t.amount if sort_by == 'amount' else t.time, reverse=rev)

        return get_user_txns_filtered_by_recipient(user, val)

    if search_by == 'direction':

        if not (val == 'outgoing' or val == 'incoming'):
            return JSONResponse(status_code=422, content='Wrong direction')

        if sort_by:
            return sorted(get_user_txns_filtered_by_dir(user, val), key=lambda t: t.amount if sort_by == 'amount' else t.time, reverse=rev)

        return get_user_txns_filtered_by_dir(user, val)

    with Session(ENGINE) as session:
        query = select(Transactions).where(or_(Transactions.sender_id == user.id, Transactions.receiver_id == user.id))
        transactions = session.exec(query).all()
        transactions_lst = []
        for txn in transactions:
            direction = 'outgoing' if user.id == txn.sender_id else 'incoming'

            if direction == 'outgoing':

                transactions_lst.append(ViewTransaction(id=txn.id, direction=direction, recipient_id=txn.receiver_id,
                                                        amount=txn.amount, time=txn.time))
            else:
                transactions_lst.append(ViewTransaction(id=txn.id, direction=direction, sender_id=txn.sender_id,
                                                        amount=txn.amount, time=txn.time))

    if sort_by == 'amount':
        transactions_lst.sort(key=lambda t: t.amount, reverse=rev)
        return transactions_lst
    elif sort_by == 'time':
        transactions_lst.sort(key=lambda t: t.time, reverse=rev)
        return transactions_lst
    else:
        return transactions_lst


def get_user_txns_filtered_by_per(user: UserCredentials | UserInfo, val: str) -> list[ViewTransaction]:
    """
    Displays the transactions of authenticated user filtered by start inclusive and end date exclusive.
    :param user: The authenticated user.
    :param val: Two date strings in the format '%Y-%m-%d' with leading zeroes and delimited with single comma and no
                whitespaces. The first str must be the earlier date.
    :return: List of filtered transactions of the logged user.
    """

    start_date, end_date = val.split(',')
    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    end_date = datetime.strptime(end_date, '%Y-%m-%d')

    with Session(ENGINE) as session:
        query = select(Transactions).where(or_(Transactions.sender_id == user.id, Transactions.receiver_id == user.id)).\
            where(Transactions.time >= start_date).where(Transactions.time <= end_date)
        transactions = session.exec(query).all()
        transactions_lst = []
        for txn in transactions:
            direction = 'outgoing' if user.id == txn.sender_id else 'incoming'

            if direction == 'outgoing':

                transactions_lst.append(ViewTransaction(id=txn.id, direction=direction, recipient_id=txn.receiver_id,
                                                        amount=txn.amount, time=txn.time))
            else:
                transactions_lst.append(ViewTransaction(id=txn.id, direction=direction, sender_id=txn.sender_id,
                                                        amount=txn.amount, time=txn.time))

    return transactions_lst


def get_user_txns_filtered_by_recipient(user: UserCredentials | UserInfo, val: str) -> list[ViewTransaction]:
    """
    Displays the transactions of authenticated user filtered by counterparty.
    :param user: The authenticated user.
    :param val: ID of the other counterparty
    :return: List of filtered transactions of the logged user.
    """
    with Session(ENGINE) as session:
        query = select(Transactions).where(or_(Transactions.sender_id == user.id, Transactions.receiver_id == user.id)).\
            where(or_(Transactions.sender_id == val, Transactions.receiver_id == val))
        transactions = session.exec(query).all()
        transactions_lst = []
        for txn in transactions:
            direction = 'outgoing' if user.id == txn.sender_id else 'incoming'

            if direction == 'outgoing':

                transactions_lst.append(ViewTransaction(id=txn.id, direction=direction, recipient_id=txn.receiver_id,
                                                        amount=txn.amount, time=txn.time))
            else:
                transactions_lst.append(ViewTransaction(id=txn.id, direction=direction, sender_id=txn.sender_id,
                                                        amount=txn.amount, time=txn.time))

    return transactions_lst


def get_user_txns_filtered_by_dir(user: UserCredentials | UserInfo, val: str) -> list[ViewTransaction]:
    """
    Displays the transactions of authenticated user filtered by direction.
    :param user: The authenticated user.
    :param val: The direction - 'incoming' or 'outgoing'
    :return: List of filtered transactions of the logged user.
    """
    with Session(ENGINE) as session:

        if val == 'outgoing':

            query = select(Transactions).where(Transactions.sender_id == user.id)
            transactions = session.exec(query).all()

        else:
            query = select(Transactions).where(Transactions.receiver_id == user.id)
            transactions = session.exec(query).all()

        transactions_lst = []

        for txn in transactions:
            direction = 'outgoing' if user.id == txn.sender_id else 'incoming'

            if direction == 'outgoing':

                transactions_lst.append(ViewTransaction(id=txn.id, direction=direction, recipient_id=txn.receiver_id,
                                                        amount=txn.amount, time=txn.time))
            else:
                transactions_lst.append(ViewTransaction(id=txn.id, direction=direction, sender_id=txn.sender_id,
                                                        amount=txn.amount, time=txn.time))

    return transactions_lst


def get_user_txns(user_id, search_by, val, sort_by, rev) -> list[ViewTransaction]:
    user = user_service.get_user_by_id(user_id)

    if search_by == 'period':

        if sort_by:
            return sorted(get_user_txns_filtered_by_per(user, val), key=lambda t: t.amount if sort_by == 'amount' else t.time, reverse=rev)

        return get_user_txns_filtered_by_per(user, val)

    if search_by == 'recipient_id':

        if sort_by:
            return sorted(get_user_txns_filtered_by_recipient(user, val), key=lambda t: t.amount if sort_by == 'amount' else t.time, reverse=rev)

        return get_user_txns_filtered_by_recipient(user, val)

    if search_by == 'direction':

        if not (val == 'outgoing' or val == 'incoming'):
            return JSONResponse(status_code=422, content='Wrong direction')

        if sort_by:
            return sorted(get_user_txns_filtered_by_dir(user, val), key=lambda t: t.amount if sort_by == 'amount' else t.time, reverse=rev)

        return get_user_txns_filtered_by_dir(user, val)

    with Session(ENGINE) as session:
        query = select(Transactions).where(or_(Transactions.sender_id == user.id, Transactions.receiver_id == user.id))
        transactions = session.exec(query).all()
        transactions_lst = []
        for txn in transactions:
            direction = 'outgoing' if user.id == txn.sender_id else 'incoming'

            if direction == 'outgoing':

                transactions_lst.append(ViewTransaction(id=txn.id, direction=direction, recipient_id=txn.receiver_id,
                                                        amount=txn.amount, time=txn.time))
            else:
                transactions_lst.append(ViewTransaction(id=txn.id, direction=direction, sender_id=txn.sender_id,
                                                        amount=txn.amount, time=txn.time))

    if sort_by == 'amount':
        transactions_lst.sort(key=lambda t: t.amount, reverse=rev)
        return transactions_lst
    elif sort_by == 'time':
        transactions_lst.sort(key=lambda t: t.time, reverse=rev)
        return transactions_lst
    else:
        return transactions_lst
