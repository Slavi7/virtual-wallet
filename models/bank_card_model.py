from datetime import date
from uuid import UUID, uuid4
from sqlmodel import SQLModel, Field as SqlField
from pydantic import BaseModel, Field, validator
from typing import Optional
from utils import currencies


class BankCardsDB(SQLModel, table=True):
    __tablename__ = "bank_cards"
    id: Optional[str] = SqlField(default_factory=lambda: str(uuid4()), primary_key=True)
    card_number: str
    card_holder: str
    ccv: str
    expiration: date
    user_id: str
    currency: str


class BankCardAdd(BaseModel):
    card_number: str = Field(min_length=16, max_length=16, regex='^([\d]+)$')
    card_holder: str = Field(min_length=2, max_length=30)
    ccv: str = Field(min_length=3, max_length=3, regex='^([\d]+)$')
    expiration: date
    currency: str

    @validator('currency')
    def is_curr_valid(cls, v):
        v = v.upper()
        if v not in currencies.supported_currencies():
            raise ValueError('currency not valid')

        return v

    @validator('expiration')
    def is_ex_date_valid(cls, v):

        if v < date.today():
            raise ValueError('Card is expired')

        return v
