from datetime import datetime
import unittest
from unittest.mock import Mock, patch
from services import wallet_service
from models import wallet_model
from models.transfer_model import TransferMoney, Transfer
from fastapi.responses import JSONResponse
from sqlmodel import create_engine, Session

class FakeResult():

    def one_or_none():
        pass

    def all():
        pass

    def first():
        pass

    def one():
        pass

    
class FakeQuery():
    
    def query():
        pass
    
    def filter_by():
        pass
    
    def delete():
        pass


fake_engine = create_engine(f"sqlite:///fake_file_name", echo=True)

FAKE_NEW_WALLET = wallet_model.CreateWallet(wallet_name="fake_wallet",currency="BGN")

FAKE_WALLET = wallet_model.Wallets(id="13df1b91-ad69-4185-bab1-6b50c3da3082",wallet_name="fake_wallet", amount=1000,currency="BGN",admin_id="4a9d05bc-78dc-4804-b81a-d2d1efb1effc")

LIST_FAKE_DB_WALLETS = [
    wallet_model.Wallets(id="14df1b91-ad69-4185-bab1-6b50c3da3082",wallet_name="fake_wallet", amount=1000,currency="BGN",admin_id="4a9d05bc-78dc-4804-b81a-d2d1efb1effc"),
    wallet_model.Wallets(id="15df1b91-ad69-4185-bab1-6b50c3da3082",wallet_name="fake_wallet_2", amount=2000,currency="BGN",admin_id="5a9d05bc-78dc-4804-b81a-d2d1efb1effc"),
]

FAKE_TRANSFER_MONEY = TransferMoney(amount=500,bank_card="9e2730f3-2871-4486-998a-9133d83bde54")

FAKE_TRANSFER = Transfer(id=1,
                         user_id="0a589ba2-2b0d-484b-9943-669154ee2256",
                         bank_cards_id="9e2730f3-2871-4486-998a-9133d83bde54",
                         wallets_id="950b5e11-2fe6-4415-96ba-6cab108d4e9a",
                         amount=1000,
                         time=datetime.now())

FAKE_WALLETS_IDS = ["14df1b91-ad69-4185-bab1-6b50c3da3082", "15df1b91-ad69-4185-bab1-6b50c3da3082"]

class CategoryWallets_serviceShould(unittest.TestCase):
    
    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"all", Mock(return_value=LIST_FAKE_DB_WALLETS))
    def test_get_all_wallets_return_listOfWallets_on_success(self):
        #act
        result: list[wallet_model.Wallets] = wallet_service.get_all_wallets()

        #assert
        self.assertEqual(len(result), 2)

class CategoryWallets_serviceShould(unittest.TestCase):
    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"all", Mock(return_value=[]))
    def test_get_all_wallets_return_emptyList_when_noWalletsfound(self):
        #act
        result: list[wallet_model.Wallets] = wallet_service.get_all_wallets()

        #assert
        self.assertEqual(len(result), 0)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"first", Mock(return_value=LIST_FAKE_DB_WALLETS[0]))
    def test_get_by_id_return_wallet_on_success(self):
        #act
        result: wallet_model.Wallets = wallet_service.get_by_id(id="1")

        #assert
        self.assertIsInstance(result, wallet_model.Wallets)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"first", Mock(return_value=None))
    def test_get_by_id_return_None_if_idNotFound(self):
        #act
        result: wallet_model.Wallets = wallet_service.get_by_id(id="1111")

        #assert
        self.assertIsNone(result)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"add", Mock(return_value="ok"))
    @patch.object(Session,"commit", Mock(return_value="ok"))
    @patch.object(Session,"refresh", Mock(return_value=LIST_FAKE_DB_WALLETS[0]))
    def test_create_wallet_return_wallet_on_success(self):
        #act
        result: wallet_model.Wallets = wallet_service.create_wallet(new_wallet=FAKE_NEW_WALLET,
                                                                    admin_id="4a9d05bc-78dc-4804-b81a-d2d1efb1effc")

        #assert
        self.assertIsInstance(result, wallet_model.Wallets)
        self.assertEqual(result.admin_id, result.admin_id)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"one", Mock(return_value=LIST_FAKE_DB_WALLETS[0]))
    @patch.object(Session,"add", Mock(return_value="ok"))
    @patch.object(Session,"commit", Mock(return_value="ok"))
    @patch.object(Session,"refresh", Mock(return_value=FAKE_TRANSFER))
    def test_add_amount_in_wallet_return_transfer_on_success(self):
        #act
        result = wallet_service.add_amount_in_wallet(user_id="4a9d05bc-78dc-4804-b81a-d2d1efb1effc",
                                                     wallet_id="14df1b91-ad69-4185-bab1-6b50c3da3082",
                                                     changed_wallet=FAKE_TRANSFER_MONEY)

        #assert
        self.assertIsInstance(result, Transfer)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"query", Mock(return_value=FakeQuery()))
    @patch.object(FakeQuery,"filter_by", Mock(return_value=FakeQuery()))
    @patch.object(FakeQuery,"delete", Mock(return_value=1))
    @patch.object(Session,"commit", Mock(return_value="ok"))
    def test_delete_wallet_return_JSONResponse_on_success(self):
        #act
        result: wallet_model.Wallets = wallet_service.delete_wallet(id="14df1b91-ad69-4185-bab1-6b50c3da3082")

        #assert
        self.assertIsInstance(result, JSONResponse)
        self.assertEqual(result.status_code, 200)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"all", Mock(return_value=LIST_FAKE_DB_WALLETS))
    def test_get_all_wallets_for_auth_user_return_listOfWallets_on_success(self):
        #act
        result: list[wallet_model.Wallets] = wallet_service.get_all_wallets_for_auth_user(user_id="fake_user_id")

        #assert
        self.assertEqual(len(result), 2)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"all", Mock(return_value=[]))
    def test_get_all_wallets_for_auth_user_return_emptyList_when_noWalletsfound(self):
        #act
        result: list[wallet_model.Wallets] = wallet_service.get_all_wallets_for_auth_user(user_id="fake_user_id")

        #assert
        self.assertEqual(len(result), 0)


    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"all", Mock(return_value=["id_1","id_2"]))
    def test_get_all_wallets_ids_for_auth_user_return_listOfWallets_on_success(self):
        #act
        result: list[wallet_model.Wallets] = wallet_service.get_all_wallet_ids_for_auth_user(user_id="fake_user_id")

        #assert
        self.assertEqual(len(result), 2)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"all", Mock(return_value=[]))
    def test_get_all_wallets_ids_for_auth_user_return_emptyList_on_success(self):
        #act
        result: list[wallet_model.Wallets] = wallet_service.get_all_wallet_ids_for_auth_user(user_id="fake_user_id")

        #assert
        self.assertEqual(len(result), 0)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"all", Mock(return_value=["id_1","id_2"]))
    def test_get_all_bank_card_ids_for_auth_user_return_listOfWallets_on_success(self):
        #act
        result: list[wallet_model.Wallets] = wallet_service.get_all_wallet_ids_for_auth_user(user_id="fake_user_id")

        #assert
        self.assertEqual(len(result), 2)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"all", Mock(return_value=[]))
    def test_get_all_bank_card_ids_for_auth_user_return_emptyList_on_success(self):
        #act
        result: list[wallet_model.Wallets] = wallet_service.get_all_wallet_ids_for_auth_user(user_id="fake_user_id")

        #assert
        self.assertEqual(len(result), 0)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"one", Mock(return_value=LIST_FAKE_DB_WALLETS[0]))
    @patch.object(Session,"add", Mock(return_value="ok"))
    @patch.object(Session,"commit", Mock(return_value="ok"))
    @patch.object(Session,"refresh", Mock(return_value=FAKE_TRANSFER))
    def test_wallet_withdraw_return_transfer_on_success(self):
        #act
        result = wallet_service.wallet_withdraw(user_id="4a9d05bc-78dc-4804-b81a-d2d1efb1effc",
                                                     wallet_id="14df1b91-ad69-4185-bab1-6b50c3da3082",
                                                     changed_wallet=FAKE_TRANSFER_MONEY)

        #assert
        self.assertIsInstance(result, Transfer)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"all", Mock(return_value=[FAKE_TRANSFER]))
    def test_get_all_transfers_for_auth_user_return_listOfTransfer_on_success(self):
        #act
        result: list[Transfer] = wallet_service.get_all_transfers_for_auth_user(user_id="fake_user_id")

        #assert
        self.assertEqual(len(result), 1)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"all", Mock(return_value=[]))
    def test_get_all_transfers_for_auth_user_return_emptyList_on_success(self):
        #act
        result: list[Transfer] = wallet_service.get_all_transfers_for_auth_user(user_id="fake_user_id")

        #assert
        self.assertEqual(len(result), 0)

    @patch("services.wallet_service.ENGINE", Mock(return_value=fake_engine))
    @patch.object(Session,"exec", Mock(return_value=FakeResult()))
    @patch.object(FakeResult,"first", Mock(return_value="100.0"))
    def test_get_wallet_amount_for_auth_user_return_string_on_success(self):
        #act
        result: float = wallet_service.get_wallet_amount_for_auth_user(user_id="fake_user_id",
                                                                                wallet_id="1")

        #assert
        self.assertIsInstance(result,str)
        self.assertEqual(result, "100.0")

    @patch("services.wallet_service.get_by_id", Mock(return_value=LIST_FAKE_DB_WALLETS[0]))
    @patch("services.wallet_service.get_all_wallet_ids_for_auth_user", Mock(return_value=FAKE_WALLETS_IDS))
    def test_check_if_user_owns_wallet_return_Wallet_if_userHasWallet(self):
        #act
        result = wallet_service.check_if_user_owns_wallet(user_id="fake_user_id",
                                                          wallet_id="1")

        #assert
        self.assertIsInstance(result, wallet_model.Wallets)

    @patch("services.wallet_service.get_by_id", Mock(return_value=None))
    @patch("services.wallet_service.get_all_wallet_ids_for_auth_user", Mock(return_value=FAKE_WALLETS_IDS))
    def test_check_if_user_owns_wallet_return_False_if_WalletIdNotFound(self):
        #act
        result = wallet_service.check_if_user_owns_wallet(user_id="fake_user_id",
                                                          wallet_id="1")

        #assert
        self.assertFalse(result)

    @patch("services.wallet_service.get_by_id", Mock(return_value=FAKE_WALLET))
    @patch("services.wallet_service.get_all_wallet_ids_for_auth_user", Mock(return_value=FAKE_WALLETS_IDS))
    def test_check_if_user_owns_wallet_return_False_if_WalletIdNotFound(self):
        #act
        result = wallet_service.check_if_user_owns_wallet(user_id="fake_user_id",
                                                          wallet_id="1")

        #assert
        self.assertFalse(result)